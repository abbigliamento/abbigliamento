﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace NegozioAbbigliamento
{
    public partial class FrmCarrello : Form
    {
        private string clienteEmail;

        public FrmCarrello(string email)
        {
            InitializeComponent();
            this.clienteEmail = email; // Memorizzo email
        }

        private void btAnnullaOrdine_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btProcediOrdine_Click(object sender, EventArgs e)
        {
            FrmPagamento frmPagamento = new FrmPagamento(this.clienteEmail);
            DialogResult dr = frmPagamento.ShowDialog(this);
        }
        private void FrmCarrello_Load(object sender, EventArgs e)
        {

        }
        public void AggiungiProdottoAlCarrello(ClsProdottoCatalogo prodotto)
        {
            // Aggiungere il prodotto al DataGridView del carrello
            DataTable dt = (DataTable)dgvVisualizzaCarrello.DataSource;

            if (dt == null)
            {
                // Se il DataTable non esiste, crealo
                dt = new DataTable();
                dt.Columns.Add("Nome");
                dt.Columns.Add("Prezzo");
                dgvVisualizzaCarrello.DataSource = dt;
            }

            // Aggiungi la riga con i dati del prodotto
            dt.Rows.Add(prodotto.Nome, prodotto.Prezzo);
        }
    }
}
