﻿namespace NegozioAbbigliamento
{
    partial class FrmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btVisualizzaOrdini = new System.Windows.Forms.Button();
            this.btInserisciBrand = new System.Windows.Forms.Button();
            this.btInserisciProdotto = new System.Windows.Forms.Button();
            this.btInserisciCategoria = new System.Windows.Forms.Button();
            this.btEsci = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btVisualizzaOrdini
            // 
            this.btVisualizzaOrdini.BackColor = System.Drawing.SystemColors.Menu;
            this.btVisualizzaOrdini.Location = new System.Drawing.Point(403, 222);
            this.btVisualizzaOrdini.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btVisualizzaOrdini.Name = "btVisualizzaOrdini";
            this.btVisualizzaOrdini.Size = new System.Drawing.Size(219, 103);
            this.btVisualizzaOrdini.TabIndex = 11;
            this.btVisualizzaOrdini.Text = "VISUALIZZA ORDINI";
            this.btVisualizzaOrdini.UseVisualStyleBackColor = false;
            this.btVisualizzaOrdini.Click += new System.EventHandler(this.btVisualizzaOrdini_Click);
            // 
            // btInserisciBrand
            // 
            this.btInserisciBrand.BackColor = System.Drawing.SystemColors.Menu;
            this.btInserisciBrand.Location = new System.Drawing.Point(179, 222);
            this.btInserisciBrand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btInserisciBrand.Name = "btInserisciBrand";
            this.btInserisciBrand.Size = new System.Drawing.Size(219, 103);
            this.btInserisciBrand.TabIndex = 10;
            this.btInserisciBrand.Text = "INSERISCI BRAND";
            this.btInserisciBrand.UseVisualStyleBackColor = false;
            this.btInserisciBrand.Click += new System.EventHandler(this.btInserisciBrand_Click);
            // 
            // btInserisciProdotto
            // 
            this.btInserisciProdotto.BackColor = System.Drawing.SystemColors.Menu;
            this.btInserisciProdotto.Location = new System.Drawing.Point(179, 112);
            this.btInserisciProdotto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btInserisciProdotto.Name = "btInserisciProdotto";
            this.btInserisciProdotto.Size = new System.Drawing.Size(219, 103);
            this.btInserisciProdotto.TabIndex = 9;
            this.btInserisciProdotto.Text = "INSERISCI PRODOTTO";
            this.btInserisciProdotto.UseVisualStyleBackColor = false;
            this.btInserisciProdotto.Click += new System.EventHandler(this.btInserisciProdotto_Click);
            // 
            // btInserisciCategoria
            // 
            this.btInserisciCategoria.BackColor = System.Drawing.SystemColors.Menu;
            this.btInserisciCategoria.Location = new System.Drawing.Point(403, 113);
            this.btInserisciCategoria.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btInserisciCategoria.Name = "btInserisciCategoria";
            this.btInserisciCategoria.Size = new System.Drawing.Size(219, 103);
            this.btInserisciCategoria.TabIndex = 8;
            this.btInserisciCategoria.Text = "INSERISCI CATEGORIA";
            this.btInserisciCategoria.UseVisualStyleBackColor = false;
            this.btInserisciCategoria.Click += new System.EventHandler(this.btInserisciCategoria_Click);
            // 
            // btEsci
            // 
            this.btEsci.BackColor = System.Drawing.SystemColors.Menu;
            this.btEsci.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEsci.Location = new System.Drawing.Point(740, 2);
            this.btEsci.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEsci.Name = "btEsci";
            this.btEsci.Size = new System.Drawing.Size(67, 30);
            this.btEsci.TabIndex = 15;
            this.btEsci.Text = "Esci";
            this.btEsci.UseVisualStyleBackColor = false;
            this.btEsci.Click += new System.EventHandler(this.btEsci_Click);
            // 
            // FrmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(809, 432);
            this.Controls.Add(this.btEsci);
            this.Controls.Add(this.btVisualizzaOrdini);
            this.Controls.Add(this.btInserisciBrand);
            this.Controls.Add(this.btInserisciProdotto);
            this.Controls.Add(this.btInserisciCategoria);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmAdmin";
            this.Text = "FrmAdmin";
            this.Load += new System.EventHandler(this.FrmAdmin_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btVisualizzaOrdini;
        private System.Windows.Forms.Button btInserisciBrand;
        private System.Windows.Forms.Button btInserisciProdotto;
        private System.Windows.Forms.Button btInserisciCategoria;
        private System.Windows.Forms.Button btEsci;
    }
}