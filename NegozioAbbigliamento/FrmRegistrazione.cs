﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;     // Questa libreria ci permette di lavorare con il file di configurazione (in alternativa posso usare Properties > Settings)
using MySqlConnector;

namespace NegozioAbbigliamento
{
    public partial class FrmRegistrazione : Form
    {
        public ClsUtente clsUtente;
        public ClsAdmin clsAdmin;
        public ClsCliente clsCliente;
        public FrmRegistrazione()
        {
            InitializeComponent();
        }

        private void btnRegistrati_Click(object sender, EventArgs e)
        {
            if(tbEmail.Text == "admin" && tbPassword.Text == "admin")
            {
                //Vengono richiamati i metodi messi a disposizione dal DBManager
                DBManager dbm = new DBManager();

                string errore = "";

                string sql = "INSERT INTO admin (email, cognome, nome, password) values (@email, @cognome, @nome, @password)";

                MySqlParameter[] parametri =
                {
                new MySqlParameter("@email", tbEmail.Text),
                new MySqlParameter("@cognome", tbCognome.Text),
                new MySqlParameter("@nome", tbNome.Text),
                new MySqlParameter("@password", tbPassword.Text)
                };

                long num = dbm.GetAffectedRows(sql, parametri, ref errore);

                if (string.IsNullOrEmpty(errore))
                {
                    MessageBox.Show($"LastID: {num}");
                    this.Close();
                }
                else
                {
                    MessageBox.Show($"Errore: {errore}");
                }

            }
            else
            {
                //Vengono richiamati i metodi messi a disposizione dal DBManager
                DBManager dbm = new DBManager();

                string errore = "";

                string sql = "INSERT INTO clienti (email, cognome, nome, password) values (@email, @cognome, @nome, @password)";

                MySqlParameter[] parametri =
                {
                new MySqlParameter("@email", tbEmail.Text),
                new MySqlParameter("@cognome", tbCognome.Text),
                new MySqlParameter("@nome", tbNome.Text),
                new MySqlParameter("@password", tbPassword.Text)
                };

                long num = dbm.GetAffectedRows(sql, parametri, ref errore);

                if (string.IsNullOrEmpty(errore))
                {
                    MessageBox.Show($"LastID: {num}");
                    this.Close();
                }
                else
                    MessageBox.Show($"Errore: {errore}");
            }
        }
        
        private void btnAnnulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmRegistrazione_Load(object sender, EventArgs e)
        {

        }

        private void pbOcchio_Click(object sender, EventArgs e)
        {
            if (tbPassword.UseSystemPasswordChar)
            {
                tbPassword.UseSystemPasswordChar = false; // Mostra la password
            }
            else
            {
                tbPassword.UseSystemPasswordChar = true; // Nascondi la password
            }
        }
    }
}
