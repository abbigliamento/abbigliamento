﻿namespace NegozioAbbigliamento
{
    partial class FrmProdotto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlProdotto = new System.Windows.Forms.Panel();
            this.cbCategoria = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbBrand = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.tbPrezzoProdotto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btSalva = new System.Windows.Forms.Button();
            this.tbNomeProdotto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlProdotto.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlProdotto
            // 
            this.pnlProdotto.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlProdotto.Controls.Add(this.cbCategoria);
            this.pnlProdotto.Controls.Add(this.label5);
            this.pnlProdotto.Controls.Add(this.cbBrand);
            this.pnlProdotto.Controls.Add(this.label4);
            this.pnlProdotto.Controls.Add(this.btAnnulla);
            this.pnlProdotto.Controls.Add(this.tbPrezzoProdotto);
            this.pnlProdotto.Controls.Add(this.label3);
            this.pnlProdotto.Controls.Add(this.btSalva);
            this.pnlProdotto.Controls.Add(this.tbNomeProdotto);
            this.pnlProdotto.Controls.Add(this.label2);
            this.pnlProdotto.Controls.Add(this.label1);
            this.pnlProdotto.Location = new System.Drawing.Point(209, 79);
            this.pnlProdotto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlProdotto.Name = "pnlProdotto";
            this.pnlProdotto.Size = new System.Drawing.Size(383, 427);
            this.pnlProdotto.TabIndex = 4;
            this.pnlProdotto.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlProdotto_Paint);
            // 
            // cbCategoria
            // 
            this.cbCategoria.FormattingEnabled = true;
            this.cbCategoria.Location = new System.Drawing.Point(80, 316);
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Size = new System.Drawing.Size(224, 24);
            this.cbCategoria.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(77, 287);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 25);
            this.label5.TabIndex = 10;
            this.label5.Text = "Categoria";
            // 
            // cbBrand
            // 
            this.cbBrand.FormattingEnabled = true;
            this.cbBrand.Location = new System.Drawing.Point(80, 249);
            this.cbBrand.Name = "cbBrand";
            this.cbBrand.Size = new System.Drawing.Size(224, 24);
            this.cbBrand.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(77, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "Brand";
            // 
            // btAnnulla
            // 
            this.btAnnulla.BackColor = System.Drawing.SystemColors.Menu;
            this.btAnnulla.Location = new System.Drawing.Point(80, 358);
            this.btAnnulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(101, 28);
            this.btAnnulla.TabIndex = 6;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = false;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // tbPrezzoProdotto
            // 
            this.tbPrezzoProdotto.Location = new System.Drawing.Point(80, 183);
            this.tbPrezzoProdotto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPrezzoProdotto.Name = "tbPrezzoProdotto";
            this.tbPrezzoProdotto.Size = new System.Drawing.Size(224, 22);
            this.tbPrezzoProdotto.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(77, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 25);
            this.label3.TabIndex = 6;
            this.label3.Text = "Prezzo";
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.SystemColors.Menu;
            this.btSalva.Location = new System.Drawing.Point(203, 358);
            this.btSalva.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(101, 28);
            this.btSalva.TabIndex = 5;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // tbNomeProdotto
            // 
            this.tbNomeProdotto.Location = new System.Drawing.Point(80, 121);
            this.tbNomeProdotto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNomeProdotto.Name = "tbNomeProdotto";
            this.tbNomeProdotto.Size = new System.Drawing.Size(224, 22);
            this.tbNomeProdotto.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(77, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(75, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 61);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prodotto";
            // 
            // FrmProdotto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(800, 582);
            this.Controls.Add(this.pnlProdotto);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmProdotto";
            this.Text = "FrmProdotto";
            this.Load += new System.EventHandler(this.FrmProdotto_Load);
            this.pnlProdotto.ResumeLayout(false);
            this.pnlProdotto.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlProdotto;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.TextBox tbPrezzoProdotto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.TextBox tbNomeProdotto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbBrand;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbCategoria;
        private System.Windows.Forms.Label label5;
    }
}