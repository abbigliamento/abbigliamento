﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace NegozioAbbigliamento
{
    public partial class FrmAdmin : Form
    {
        public FrmAdmin()
        {
            InitializeComponent();
        }
        
        private void btInserisciProdotto_Click(object sender, EventArgs e)
        {
            FrmProdotto frmProdotto = new FrmProdotto();
            DialogResult dr = frmProdotto.ShowDialog(this);
        }

        private void btInserisciCategoria_Click(object sender, EventArgs e)
        {
            FrmCategoria frmCategoria = new FrmCategoria();
            DialogResult dr = frmCategoria.ShowDialog(this);
        }

        private void btInserisciBrand_Click(object sender, EventArgs e)
        {
            FrmBrand frmbrand = new FrmBrand();
            DialogResult dr = frmbrand.ShowDialog(this);

        }

        private void btVisualizzaOrdini_Click(object sender, EventArgs e)
        {
            FrmInsiemeOrdini frmInsiemeOrdini = new FrmInsiemeOrdini();
            DialogResult dr = frmInsiemeOrdini.ShowDialog(this);
        }
        private void FrmAdmin_Load(object sender, EventArgs e)
        {
            
        }

        private void btEsci_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
