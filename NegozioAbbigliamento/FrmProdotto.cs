﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using MySqlConnector;

namespace NegozioAbbigliamento
{
    public partial class FrmProdotto : Form
    {
        public FrmProdotto()
        {
            InitializeComponent();
        }

        private void btAnnulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btSalva_Click(object sender, EventArgs e)
        {
            //INSERIRE CODICE
            //Vengono richiamati i metodi messi a disposizione dal DBManager
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "INSERT INTO prodotticatalogo (nome, prezzo, brandNome, categoriaNome, adminEmail) values (@nome, @prezzo, @brandNome, @categoriaNome, @adminEmail)";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@nome", tbNomeProdotto.Text),
                new MySqlParameter("@prezzo", tbPrezzoProdotto.Text),
                new MySqlParameter("@brandNome", cbBrand.SelectedValue),
                new MySqlParameter("@categoriaNome", cbCategoria.SelectedValue),
                new MySqlParameter("@adminEmail", "admin")
            };

            long num = dbm.GetAffectedRows(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
                MessageBox.Show($"LastID: {num}");
            else
                MessageBox.Show($"Errore: {errore}");

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void FrmProdotto_Load(object sender, EventArgs e)
        {
            BrandProdotto();
            CategoriaProdotto();
        }

        private void CategoriaProdotto()
        {
            DBManager dbm = new DBManager();
            string errore = "";

            string sql = "SELECT nome FROM categorie"; // Nessun parametro necessario

            DataTable dt = dbm.GetDataTableByQuery(sql, null, ref errore); // Passa null come parametri

            if (string.IsNullOrEmpty(errore))
            {
                cbCategoria.DataSource = dt;
                cbCategoria.DisplayMember = "nome"; // Assicurati che il campo esista nella tabella
                cbCategoria.ValueMember = "nome"; // Puoi cambiarlo in un altro campo se necessario
            }
            else
            {
                MessageBox.Show($"Errore: {errore}");
            }
        }

        private void BrandProdotto()
        {
            DBManager dbm = new DBManager();
            string errore = "";

            string sql = "SELECT * FROM brand"; // Nessun parametro necessario

            DataTable dt = dbm.GetDataTableByQuery(sql, null, ref errore); // Passa null come parametri

            if (string.IsNullOrEmpty(errore))
            {
                cbBrand.DataSource = dt;
                cbBrand.DisplayMember = "nome"; // Assicurati che il campo esista nella tabella
                cbBrand.ValueMember = "nome"; // Puoi cambiarlo in un altro campo se necessario
            }
            else
            {
                MessageBox.Show($"Errore: {errore}");
            }
        }

        private void pnlProdotto_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
