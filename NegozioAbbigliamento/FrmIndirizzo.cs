﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using MySqlConnector;

namespace NegozioAbbigliamento
{
    public partial class FrmIndirizzo : Form
    {
        private string clienteEmail;
        public FrmIndirizzo(string email)
        {
            InitializeComponent();
            this.clienteEmail = email; // Memorizza l'email
        }

        private void btnAnnulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSalva_Click(object sender, EventArgs e)
        {
            //Vengono richiamati i metodi messi a disposizione dal DBManager
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "INSERT INTO indirizzi (via, citta, stato, CAP, clienteEmail) values (@via, @citta, @stato, @CAP, @clienteEmail)";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@via", tbVia.Text),
                new MySqlParameter("@citta", tbCitta.Text),
                new MySqlParameter("@stato", tbStato.Text),
                new MySqlParameter("@CAP", mtbCAP.Text),
                new MySqlParameter("@clienteEmail", this.clienteEmail)
            };

            long num = dbm.GetAffectedRows(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
                MessageBox.Show($"LastID: {num}");
            else
                MessageBox.Show($"Errore: {errore}");
        }
        private void FrmIndirizzo_Load(object sender, EventArgs e)
        {
            
        }
    }
}
