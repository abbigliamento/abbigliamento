﻿namespace NegozioAbbigliamento
{
    partial class FrmCarrello
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.btAnnullaOrdine = new System.Windows.Forms.Button();
            this.btProcediOrdine = new System.Windows.Forms.Button();
            this.pnlCarrello = new System.Windows.Forms.Panel();
            this.dgvVisualizzaCarrello = new System.Windows.Forms.DataGridView();
            this.pnlCarrello.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisualizzaCarrello)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(164, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 61);
            this.label2.TabIndex = 10;
            this.label2.Text = "Carrello";
            // 
            // btAnnullaOrdine
            // 
            this.btAnnullaOrdine.BackColor = System.Drawing.SystemColors.Menu;
            this.btAnnullaOrdine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAnnullaOrdine.Location = new System.Drawing.Point(13, 418);
            this.btAnnullaOrdine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnullaOrdine.Name = "btAnnullaOrdine";
            this.btAnnullaOrdine.Size = new System.Drawing.Size(149, 103);
            this.btAnnullaOrdine.TabIndex = 9;
            this.btAnnullaOrdine.Text = "Annulla ordine";
            this.btAnnullaOrdine.UseVisualStyleBackColor = false;
            this.btAnnullaOrdine.Click += new System.EventHandler(this.btAnnullaOrdine_Click);
            // 
            // btProcediOrdine
            // 
            this.btProcediOrdine.BackColor = System.Drawing.SystemColors.Menu;
            this.btProcediOrdine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btProcediOrdine.Location = new System.Drawing.Point(445, 418);
            this.btProcediOrdine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btProcediOrdine.Name = "btProcediOrdine";
            this.btProcediOrdine.Size = new System.Drawing.Size(149, 103);
            this.btProcediOrdine.TabIndex = 8;
            this.btProcediOrdine.Text = "Procedi all\'ordine";
            this.btProcediOrdine.UseVisualStyleBackColor = false;
            this.btProcediOrdine.Click += new System.EventHandler(this.btProcediOrdine_Click);
            // 
            // pnlCarrello
            // 
            this.pnlCarrello.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlCarrello.Controls.Add(this.dgvVisualizzaCarrello);
            this.pnlCarrello.Location = new System.Drawing.Point(13, 73);
            this.pnlCarrello.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlCarrello.Name = "pnlCarrello";
            this.pnlCarrello.Size = new System.Drawing.Size(581, 340);
            this.pnlCarrello.TabIndex = 7;
            // 
            // dgvVisualizzaCarrello
            // 
            this.dgvVisualizzaCarrello.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvVisualizzaCarrello.BackgroundColor = System.Drawing.Color.White;
            this.dgvVisualizzaCarrello.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVisualizzaCarrello.Location = new System.Drawing.Point(0, 0);
            this.dgvVisualizzaCarrello.Margin = new System.Windows.Forms.Padding(4);
            this.dgvVisualizzaCarrello.Name = "dgvVisualizzaCarrello";
            this.dgvVisualizzaCarrello.Size = new System.Drawing.Size(581, 339);
            this.dgvVisualizzaCarrello.TabIndex = 16;
            // 
            // FrmCarrello
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(608, 546);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btAnnullaOrdine);
            this.Controls.Add(this.btProcediOrdine);
            this.Controls.Add(this.pnlCarrello);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmCarrello";
            this.Text = "FrmCarrello";
            this.Load += new System.EventHandler(this.FrmCarrello_Load);
            this.pnlCarrello.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisualizzaCarrello)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btAnnullaOrdine;
        private System.Windows.Forms.Button btProcediOrdine;
        private System.Windows.Forms.Panel pnlCarrello;
        private System.Windows.Forms.DataGridView dgvVisualizzaCarrello;
    }
}