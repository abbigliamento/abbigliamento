﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using MySqlConnector;  // Include dll opportuna per lavorare con MySql


namespace NegozioAbbigliamento
{
    public partial class FrmCategoria : Form
    {
        public FrmCategoria()
        {
            InitializeComponent();
        }

        private void btAnnulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FrmCategoria_Load(object sender, EventArgs e)
        {
            
        }

        private void btSalva_Click(object sender, EventArgs e)
        {

            //Vengono richiamati i metodi messi a disposizione dal DBManager
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "INSERT INTO categorie (nome, adminEmail) values (@nome, @adminEmail)";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@nome", tbNomeCategoria.Text),
                new MySqlParameter("@adminEmail", "admin")
            };

            long num = dbm.GetAffectedRows(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
                MessageBox.Show($"LastID: {num}");
            else
                MessageBox.Show($"Errore: {errore}");

        }
    }
}
