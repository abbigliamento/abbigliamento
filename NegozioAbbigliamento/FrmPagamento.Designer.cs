﻿namespace NegozioAbbigliamento
{
    partial class FrmPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSpedizione = new System.Windows.Forms.Panel();
            this.cbIndirizzo = new System.Windows.Forms.ComboBox();
            this.btAggiungiIndirizzo = new System.Windows.Forms.Button();
            this.btSelezionaIndirizzo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbCarta = new System.Windows.Forms.ComboBox();
            this.btAggiungiCarta = new System.Windows.Forms.Button();
            this.btOrdinaCarta = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlSpedizione.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSpedizione
            // 
            this.pnlSpedizione.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlSpedizione.Controls.Add(this.cbIndirizzo);
            this.pnlSpedizione.Controls.Add(this.btAggiungiIndirizzo);
            this.pnlSpedizione.Controls.Add(this.btSelezionaIndirizzo);
            this.pnlSpedizione.Controls.Add(this.label1);
            this.pnlSpedizione.Controls.Add(this.label2);
            this.pnlSpedizione.Location = new System.Drawing.Point(31, 14);
            this.pnlSpedizione.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlSpedizione.Name = "pnlSpedizione";
            this.pnlSpedizione.Size = new System.Drawing.Size(457, 238);
            this.pnlSpedizione.TabIndex = 8;
            // 
            // cbIndirizzo
            // 
            this.cbIndirizzo.FormattingEnabled = true;
            this.cbIndirizzo.Location = new System.Drawing.Point(109, 121);
            this.cbIndirizzo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbIndirizzo.Name = "cbIndirizzo";
            this.cbIndirizzo.Size = new System.Drawing.Size(223, 24);
            this.cbIndirizzo.TabIndex = 22;
            // 
            // btAggiungiIndirizzo
            // 
            this.btAggiungiIndirizzo.BackColor = System.Drawing.SystemColors.Menu;
            this.btAggiungiIndirizzo.Location = new System.Drawing.Point(67, 160);
            this.btAggiungiIndirizzo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAggiungiIndirizzo.Name = "btAggiungiIndirizzo";
            this.btAggiungiIndirizzo.Size = new System.Drawing.Size(127, 46);
            this.btAggiungiIndirizzo.TabIndex = 16;
            this.btAggiungiIndirizzo.Text = "Aggiungi";
            this.btAggiungiIndirizzo.UseVisualStyleBackColor = false;
            this.btAggiungiIndirizzo.Click += new System.EventHandler(this.btAggiungiIndirizzo_Click);
            // 
            // btSelezionaIndirizzo
            // 
            this.btSelezionaIndirizzo.BackColor = System.Drawing.SystemColors.Menu;
            this.btSelezionaIndirizzo.Location = new System.Drawing.Point(245, 160);
            this.btSelezionaIndirizzo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSelezionaIndirizzo.Name = "btSelezionaIndirizzo";
            this.btSelezionaIndirizzo.Size = new System.Drawing.Size(123, 46);
            this.btSelezionaIndirizzo.TabIndex = 15;
            this.btSelezionaIndirizzo.Text = "Seleziona";
            this.btSelezionaIndirizzo.UseVisualStyleBackColor = false;
            this.btSelezionaIndirizzo.Click += new System.EventHandler(this.btSelezionaIndirizzo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(105, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Seleziona indirizzo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(72, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(295, 61);
            this.label2.TabIndex = 0;
            this.label2.Text = "Spedizione";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.panel1.Controls.Add(this.cbCarta);
            this.panel1.Controls.Add(this.btAggiungiCarta);
            this.panel1.Controls.Add(this.btOrdinaCarta);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Location = new System.Drawing.Point(508, 14);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(457, 238);
            this.panel1.TabIndex = 7;
            // 
            // cbCarta
            // 
            this.cbCarta.FormattingEnabled = true;
            this.cbCarta.Location = new System.Drawing.Point(109, 121);
            this.cbCarta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCarta.Name = "cbCarta";
            this.cbCarta.Size = new System.Drawing.Size(223, 24);
            this.cbCarta.TabIndex = 22;
            // 
            // btAggiungiCarta
            // 
            this.btAggiungiCarta.BackColor = System.Drawing.SystemColors.Menu;
            this.btAggiungiCarta.Location = new System.Drawing.Point(83, 160);
            this.btAggiungiCarta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAggiungiCarta.Name = "btAggiungiCarta";
            this.btAggiungiCarta.Size = new System.Drawing.Size(127, 46);
            this.btAggiungiCarta.TabIndex = 16;
            this.btAggiungiCarta.Text = "Aggiungi";
            this.btAggiungiCarta.UseVisualStyleBackColor = false;
            this.btAggiungiCarta.Click += new System.EventHandler(this.btAggiungiCarta_Click);
            // 
            // btOrdinaCarta
            // 
            this.btOrdinaCarta.BackColor = System.Drawing.SystemColors.Menu;
            this.btOrdinaCarta.Location = new System.Drawing.Point(253, 160);
            this.btOrdinaCarta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btOrdinaCarta.Name = "btOrdinaCarta";
            this.btOrdinaCarta.Size = new System.Drawing.Size(123, 46);
            this.btOrdinaCarta.TabIndex = 15;
            this.btOrdinaCarta.Text = "Ordina";
            this.btOrdinaCarta.UseVisualStyleBackColor = false;
            this.btOrdinaCarta.Click += new System.EventHandler(this.btOrdinaCarta_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(105, 94);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 25);
            this.label11.TabIndex = 1;
            this.label11.Text = "Seleziona carta";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(72, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(304, 61);
            this.label12.TabIndex = 0;
            this.label12.Text = "Pagamento";
            // 
            // FrmPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(980, 294);
            this.Controls.Add(this.pnlSpedizione);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmPagamento";
            this.Text = "Pagamento";
            this.Load += new System.EventHandler(this.FrmPagamento_Load);
            this.pnlSpedizione.ResumeLayout(false);
            this.pnlSpedizione.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSpedizione;
        private System.Windows.Forms.ComboBox cbIndirizzo;
        private System.Windows.Forms.Button btAggiungiIndirizzo;
        private System.Windows.Forms.Button btSelezionaIndirizzo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbCarta;
        private System.Windows.Forms.Button btAggiungiCarta;
        private System.Windows.Forms.Button btOrdinaCarta;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}