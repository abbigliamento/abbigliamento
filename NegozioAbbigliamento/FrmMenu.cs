﻿using System;
using System.Collections.Generic;
using System.Data; // Necessario per DataTable
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace NegozioAbbigliamento
{
    public partial class FrmMenu : Form
    {

        private string clienteEmail;
        private FrmCarrello frmCarrello; // Aggiungi una variabile per la form carrello

        public FrmMenu(string email)
        {
            InitializeComponent();
            this.clienteEmail = email; // Memorizzo l'email
            frmCarrello = null; // Inizialmente la form carrello è null
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (frmCarrello == null || frmCarrello.IsDisposed)
            {
                frmCarrello = new FrmCarrello(this.clienteEmail);
                frmCarrello.Show(this);
            }
            else
            {
                frmCarrello.BringToFront();
            }
        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {

            DBManager _dbm = new DBManager();

            #region LISTA  USANDO IL GESTORE (BUSINESS LOGIC)
            //Oggetto gestore
            ClsCategoriaBL clsCategoriaBL = new ClsCategoriaBL(_dbm);
            ClsProdottoCatalogoBL clsProdottoCatalogoBL = new ClsProdottoCatalogoBL(_dbm);


            //Invocazione metodo GetLibri
            string errore = "";
            List<ClsCategoria> _listacategoria = clsCategoriaBL.GetCategorie(ref errore);
            List<ClsProdottoCatalogo> _listaprodotticatalogo = clsProdottoCatalogoBL.GetProdottiCatalogo(ref errore);


            if (string.IsNullOrEmpty(errore))
            {
                lbCategorie.DataSource = _listacategoria;
                lbCategorie.DisplayMember = "Nome";
                dgvVisualizza.DataSource = _listaprodotticatalogo;
            }
            else
            {
                MessageBox.Show($"Errore:{errore}");
            }
            #endregion
        }

        private void btEsci_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btCerca_Click(object sender, EventArgs e)
        {
            DBManager dbm = new DBManager();
            string errore = "";
            DataTable dt;

            string sql = "SELECT * FROM prodotticatalogo";
            
            if (!string.IsNullOrEmpty(tbCercaProdotto.Text))
            {
                sql += " WHERE nome LIKE @nome";
            }

            // Imposta i parametri solo se è necessario
            MySqlParameter[] parametri = new MySqlParameter[]
            {
                new MySqlParameter("@nome", tbCercaProdotto.Text)
            };

            if (string.IsNullOrEmpty(tbCercaProdotto.Text))
            {
                dt = dbm.GetDataTableByQuery(sql, null, ref errore); 
            }
            else
            {
                dt = dbm.GetDataTableByQuery(sql, parametri, ref errore); 
            }

            if (string.IsNullOrEmpty(errore))
            {
                dgvVisualizza.DataSource = dt;
            }
            else
            {
                MessageBox.Show($"Errore: {errore}");
            }
        }

        private void PopolaCarrello()
        {
            //qua bisogna che seleziono una riga dalla dgvVisualizza e nella frm carrello nella dgvVisualizzaCarrello ci mette quella riga. 

            if (dgvVisualizza.SelectedRows.Count > 0)
            {
                DataGridViewRow _rigaSelezionata = dgvVisualizza.SelectedRows[0];

                ClsProdottoCatalogo prodottoSelezionato = new ClsProdottoCatalogo
                {
                    Nome = _rigaSelezionata.Cells["Nome"].Value.ToString(),
                    Prezzo = Convert.ToDouble(_rigaSelezionata.Cells["Prezzo"].Value)
                };

                // Verifica se la form del carrello è già aperta
                if (frmCarrello == null || frmCarrello.IsDisposed)
                {
                    // Crea la form solo se non esiste ancora o è stata chiusa
                    frmCarrello = new FrmCarrello(clienteEmail);
                    frmCarrello.Show();
                }

                frmCarrello.AggiungiProdottoAlCarrello(prodottoSelezionato);

                // Porta la form carrello in primo piano
                frmCarrello.BringToFront();
            }
            else
            {
                MessageBox.Show("Seleziona un prodotto per aggiungerlo al carrello.");
            }
        }


        private void btAggiungiCarrello_Click(object sender, EventArgs e)
        {
            PopolaCarrello();
            
        }
    }
}
