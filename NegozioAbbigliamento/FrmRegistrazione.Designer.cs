﻿namespace NegozioAbbigliamento
{
    partial class FrmRegistrazione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistrazione));
            this.pnlRegistrazione = new System.Windows.Forms.Panel();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAnnulla = new System.Windows.Forms.Button();
            this.btnRegistrati = new System.Windows.Forms.Button();
            this.tbCognome = new System.Windows.Forms.TextBox();
            this.tbNome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbOcchio = new System.Windows.Forms.PictureBox();
            this.pnlRegistrazione.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOcchio)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlRegistrazione
            // 
            this.pnlRegistrazione.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlRegistrazione.Controls.Add(this.pbOcchio);
            this.pnlRegistrazione.Controls.Add(this.tbPassword);
            this.pnlRegistrazione.Controls.Add(this.tbEmail);
            this.pnlRegistrazione.Controls.Add(this.label4);
            this.pnlRegistrazione.Controls.Add(this.label5);
            this.pnlRegistrazione.Controls.Add(this.btnAnnulla);
            this.pnlRegistrazione.Controls.Add(this.btnRegistrati);
            this.pnlRegistrazione.Controls.Add(this.tbCognome);
            this.pnlRegistrazione.Controls.Add(this.tbNome);
            this.pnlRegistrazione.Controls.Add(this.label3);
            this.pnlRegistrazione.Controls.Add(this.label2);
            this.pnlRegistrazione.Controls.Add(this.label1);
            this.pnlRegistrazione.Location = new System.Drawing.Point(220, 14);
            this.pnlRegistrazione.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlRegistrazione.Name = "pnlRegistrazione";
            this.pnlRegistrazione.Size = new System.Drawing.Size(378, 479);
            this.pnlRegistrazione.TabIndex = 2;
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(78, 330);
            this.tbPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(224, 22);
            this.tbPassword.TabIndex = 11;
            this.tbPassword.UseSystemPasswordChar = true;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(78, 257);
            this.tbEmail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(224, 22);
            this.tbEmail.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(78, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(78, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 25);
            this.label5.TabIndex = 8;
            this.label5.Text = "Email";
            // 
            // btnAnnulla
            // 
            this.btnAnnulla.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAnnulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnulla.Location = new System.Drawing.Point(79, 383);
            this.btnAnnulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAnnulla.Name = "btnAnnulla";
            this.btnAnnulla.Size = new System.Drawing.Size(93, 57);
            this.btnAnnulla.TabIndex = 7;
            this.btnAnnulla.Text = "Annulla";
            this.btnAnnulla.UseVisualStyleBackColor = false;
            this.btnAnnulla.Click += new System.EventHandler(this.btnAnnulla_Click);
            // 
            // btnRegistrati
            // 
            this.btnRegistrati.BackColor = System.Drawing.SystemColors.Menu;
            this.btnRegistrati.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrati.Location = new System.Drawing.Point(207, 383);
            this.btnRegistrati.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRegistrati.Name = "btnRegistrati";
            this.btnRegistrati.Size = new System.Drawing.Size(96, 57);
            this.btnRegistrati.TabIndex = 6;
            this.btnRegistrati.Text = "Registrati";
            this.btnRegistrati.UseVisualStyleBackColor = false;
            this.btnRegistrati.Click += new System.EventHandler(this.btnRegistrati_Click);
            // 
            // tbCognome
            // 
            this.tbCognome.Location = new System.Drawing.Point(79, 187);
            this.tbCognome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCognome.Name = "tbCognome";
            this.tbCognome.Size = new System.Drawing.Size(224, 22);
            this.tbCognome.TabIndex = 4;
            // 
            // tbNome
            // 
            this.tbNome.Location = new System.Drawing.Point(79, 112);
            this.tbNome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(224, 22);
            this.tbNome.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(74, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cognome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(74, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(59, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 61);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registrati";
            // 
            // pbOcchio
            // 
            this.pbOcchio.Image = ((System.Drawing.Image)(resources.GetObject("pbOcchio.Image")));
            this.pbOcchio.Location = new System.Drawing.Point(308, 330);
            this.pbOcchio.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbOcchio.Name = "pbOcchio";
            this.pbOcchio.Size = new System.Drawing.Size(27, 22);
            this.pbOcchio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbOcchio.TabIndex = 12;
            this.pbOcchio.TabStop = false;
            this.pbOcchio.Click += new System.EventHandler(this.pbOcchio_Click);
            // 
            // FrmRegistrazione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(803, 515);
            this.Controls.Add(this.pnlRegistrazione);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmRegistrazione";
            this.Text = "Registrazione";
            this.Load += new System.EventHandler(this.FrmRegistrazione_Load);
            this.pnlRegistrazione.ResumeLayout(false);
            this.pnlRegistrazione.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOcchio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlRegistrazione;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAnnulla;
        private System.Windows.Forms.Button btnRegistrati;
        private System.Windows.Forms.TextBox tbCognome;
        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbOcchio;
    }
}