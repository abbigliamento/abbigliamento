﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using MySqlConnector;

namespace NegozioAbbigliamento
{
    public partial class FrmAccesso : Form
    {
        public FrmAccesso()
        {
            InitializeComponent();
        }

        public string ClienteEmail
        {
            get { return tbEmail.Text; }
        }

        private void btnRegistrati_Click(object sender, EventArgs e)
        {
            FrmRegistrazione frmRegistrazione = new FrmRegistrazione();
            DialogResult dr = frmRegistrazione.ShowDialog(this);
        }

        private void btnAnnulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnAccedi_Click(object sender, EventArgs e)
        {
            string _username = tbEmail.Text;
            string _password = tbPassword.Text;
            
            if (_username == "admin" && _password == "admin")
            {
                DBManager dbm = new DBManager();
                string errore = "";

                string query = "SELECT COUNT(*) FROM admin WHERE email = @username AND password = @password";

                MySqlParameter[] parametri =
                {
                    new MySqlParameter("@username", _username),
                    new MySqlParameter("@password", _password)
                };

                object risultato = dbm.GetScalarByQuery(query, parametri, ref errore);

                if (string.IsNullOrEmpty(errore))
                {
                    if (Convert.ToInt32(risultato) > 0)
                    {
                        FrmAdmin frmAdmin = new FrmAdmin();
                        frmAdmin.Show();
                    }
                    else
                    {
                        MessageBox.Show("L'admin non è registrato. Effettua la registrazione.");
                    }
                }
                else
                {
                    MessageBox.Show("Errore di connessione al database: " + errore);
                }
            }
            else
            {
                DBManager dbm = new DBManager();
                string errore = "";
                
                string query = "SELECT COUNT(*) FROM clienti WHERE email = @username AND password = @password";
                
                MySqlParameter[] parametri =
                {
                    new MySqlParameter("@username", _username),
                    new MySqlParameter("@password", _password)
                };
                
                object risultato = dbm.GetScalarByQuery(query, parametri, ref errore);

                if (string.IsNullOrEmpty(errore))
                {
                    if (Convert.ToInt32(risultato) > 0)
                    {
                        FrmMenu frmMenu = new FrmMenu(_username);
                        frmMenu.Show();
                    }
                    else
                    {
                        MessageBox.Show("L'utente non è registrato. Effettua la registrazione.");
                    }
                }
                else
                {
                    MessageBox.Show("Errore di connessione al database: " + errore);
                }
            }
        }


        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void pbOcchio_Click(object sender, EventArgs e)
        {
            if (tbPassword.UseSystemPasswordChar)
            {
                tbPassword.UseSystemPasswordChar = false; // Mostra la password
            }
            else
            {
                tbPassword.UseSystemPasswordChar = true; // Nascondi la password
            }
        }
    }
}
