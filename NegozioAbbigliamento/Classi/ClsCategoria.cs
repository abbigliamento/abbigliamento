﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegozioAbbigliamento
{
    public class ClsCategoria
    {
        //attributi
        string _nome;

        public string Nome { get => _nome; set => _nome = value; }

        public ClsCategoria()
        {

        }

        public ClsCategoria(string nome)
        {
            Nome = nome;
        }
    }
}
