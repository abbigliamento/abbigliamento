﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NegozioAbbigliamento
{
    public class ClsProdottoCatalogo
    {
        #region Attributi
        //SABBATINI
        string _nome;
        double _prezzo;

        //chiave esterna, controllare a scuola, manca proprietà

        #endregion
        #region Proprieta
        public string Nome
        {
            get
            {
                return _nome;
            }
            set
            {
                if (value.Length > 0)
                {
                    _nome = value;
                }
                else
                    value = _nome;
            }
        }
        public double Prezzo
        {
            get
            {
                return _prezzo;
            }
            set
            {
                if (value > 0)
                {
                    _prezzo = value;
                }
                else
                    value = _prezzo;
            }
        }
        #endregion

        public ClsProdottoCatalogo()
        {
           
        }
        public ClsProdottoCatalogo(string _nome, double _prezzo)
        {
            Nome = _nome;
            Prezzo = _prezzo;
        }

    }
}
