﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NegozioAbbigliamento
{
    public class ClsAdmin : ClsUtente 
    {
        //MONSU
        //attributi
        string _password = "admin";
        string _email = "admin";
        //costruttore
        public ClsAdmin(string cognome, string nome, DateTime dataNascita, string email, string password)
        {
            this.Nome = nome;
            this.Cognome = cognome;
            this.Email = email;
            this.Password = password;
            this.DataNascita = dataNascita;
        }
        public bool accedi()
        {
            for (int i = 0; i < Program._utenti.Count; i++)
            {
                if (Program._utenti[i].Email == _email && Program._utenti[i].Password == _password)
                    return true;
            }
            return false;
        }
    }
}
