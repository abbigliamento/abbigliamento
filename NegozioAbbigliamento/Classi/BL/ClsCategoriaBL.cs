﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NegozioAbbigliamento
{
    public class ClsCategoriaBL
    {
        #region Attributi
        //SABBATINI MONSU
        //attributi
        DBManager _dbm = null;
        #endregion
        #region Proprieta
        //proprieta

        #endregion
        #region Costruttore
        //costruttori
        public ClsCategoriaBL(DBManager dbm)
        {
            _dbm = dbm;
        }
        #endregion


        public List<ClsCategoria> GetCategorie(ref string errore)
        {
            DataTable dt = null;
            List<ClsCategoria> _listaCategorie = new List<ClsCategoria>();

            // 1. Sfruttando la classe DbManager
            try
            {
                string query = "SELECT * FROM categorie"; //query veloce!
                dt = _dbm.GetDataTableByQuery(query, null, ref errore);
                if (string.IsNullOrEmpty(errore))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        // Potrei scrivere anche su ua sola riga ma così è più leggibile

                        ClsCategoria _categoria = new ClsCategoria(
                            dt.Rows[i]["nome"].ToString());
                        _listaCategorie.Add(_categoria);
                    }
                }
            }
            catch (Exception ex)
            {
                errore = ex.Message;
            }

            return _listaCategorie;
        }

    }
}
