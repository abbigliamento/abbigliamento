﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace NegozioAbbigliamento
{
    public class ClsProdottoCatalogoBL
    {
        #region Attributi
        //SABBATINI MONSU
        //attributi
        DBManager _dbm = null;
        #endregion
        #region Proprieta
        //proprieta

        #endregion
        #region Costruttore
        //costruttori
        public ClsProdottoCatalogoBL(DBManager dbm)
        {
            _dbm = dbm;
        }
        #endregion


        public List<ClsProdottoCatalogo> GetProdottiCatalogo(ref string errore)
        {
            DataTable dt = null;
            List<ClsProdottoCatalogo> _listaProdottiCatalogo = new List<ClsProdottoCatalogo>();

            // 1. Sfruttando la classe DbManager
            try
            {
                string query = "SELECT * FROM prodotticatalogo"; //query veloce!
                dt = _dbm.GetDataTableByQuery(query, null, ref errore);
                if (string.IsNullOrEmpty(errore))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        // Potrei scrivere anche su ua sola riga ma così è più leggibile

                        ClsProdottoCatalogo _prodotticatalogo = new ClsProdottoCatalogo(
                            dt.Rows[i]["nome"].ToString(),
                            (double)dt.Rows[i]["prezzo"]);
                        _listaProdottiCatalogo.Add(_prodotticatalogo);
                    }
                }
            }
            catch (Exception ex)
            {
                errore = ex.Message;
            }

            return _listaProdottiCatalogo;
        }
    }
}
