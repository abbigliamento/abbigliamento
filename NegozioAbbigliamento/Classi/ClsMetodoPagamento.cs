﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NegozioAbbigliamento
{
    public class ClsMetodoPagamento
    {
        
        #region Attributi
        //MONSU
        //attributi
        int _Id;
        string _numeroCarta, _titolare, _emailCliente;
        DateTime _scadenza;
        public enum eTipo  
        {
            paypal,
            mastercard,
            postepay,
            hype
        }
        #endregion
        #region Proprieta
        //proprieta
        public int Id { get => _Id; set => _Id = value; }
        public string NumeroCarta { get => _numeroCarta; set => _numeroCarta = value; }
        public string Titolare { get => _titolare; set => _titolare = value; }
        public DateTime Scadenza { get => _scadenza; set => _scadenza = value; }
        public eTipo Tipo { get; set; }
        public string EmailCliente { get => _emailCliente; set => _emailCliente = value; }
        #endregion
        #region Costruttore
        //costruttori
        public ClsMetodoPagamento()
        {

        }
        #endregion
        #region Metodi
        public ClsMetodoPagamento(int Id, string numeroCarta, string titolare, DateTime scadenza, eTipo tipo, string emailCliente)
        {
            this._Id = Id;
            this._numeroCarta = numeroCarta;
            this._titolare = titolare;
            this._scadenza = scadenza;
            this.Tipo = tipo;
            this.EmailCliente = emailCliente;
        }
        //metodi
        public void elimina(int ID)
        {
            for (int i = 0; i < Program._metodiPagamento.Count; i++)
            {
                if (Program._metodiPagamento[i].Id == ID)
                    Program._metodiPagamento.RemoveAt(i);
            }
        }
        public void ricerca(int ID)
        {
            Program._metodiPagamentoRicerca.Clear();
            for (int i = 0; i < Program._utenti.Count; i++)
            {
                if (ID == Program._metodiPagamento[i].Id)
                    Program._metodiPagamentoRicerca.Add(Program._metodiPagamento[i]);
            }

        }
        #endregion
    }
}
