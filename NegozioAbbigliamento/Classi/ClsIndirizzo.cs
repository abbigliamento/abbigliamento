﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NegozioAbbigliamento
{
    public class ClsIndirizzo
    {
        
        #region Attributi
        //MONSU
        //attributi
        int _Id;
        string _via, _citta, _stato, Cap, _emailCliente;
        #endregion
        #region Proprieta
        //proprietà
        public int Id { get => _Id; set => _Id = value; }
        public string Via
        {
            get
            {
                return _via;
            }
            set
            {
                if (value.Length > 0)
                    _via = value;
                else
                    value = _via;

            }
        }
        public string Citta
        {
            get
            {
                return _stato;
            }
            set
            {
                if (value.Length > 0)
                    _stato = value;
                else
                    value = _stato;

            }
        }
        public string Stato
        {
            get
            {
                return _citta;
            }
            set
            {
                if (value.Length > 0)
                    _citta = value;
                else
                    value = _citta;

            }
        }
        public string CAP
        {
            get
            {
                return Cap;
            }
            set
            {
                if (value.Length == 5)
                    Cap = value;
                else
                    value = Cap;

            }
        }
        public string EmailCliente { get => _emailCliente; set => _emailCliente = value; }
        #endregion
        #region Costruttore
        //costruttori
        public ClsIndirizzo()
        {
            
        }
        #endregion
        #region Metodi
        public ClsIndirizzo(int id, string via, string citta, string stato, string cap, string emailCliente)
        {
            this.Id = id;
            this.Via = via;
            this.Citta = citta;
            this.Stato = stato;
            this.CAP = cap;
            this.EmailCliente = emailCliente;
        }
       
        public void elimina(int ID)
        {
            for (int i = 0; i < Program._indirizzi.Count; i++)
            {
                if (Program._indirizzi[i].Id == ID)
                    Program._indirizzi.RemoveAt(i);
            }
        }
        public void ricerca(int ID)
        {
            Program._indirizziRicerca.Clear();
            for (int i = 0; i < Program._utenti.Count; i++)
            {
                if (ID == Program._indirizzi[i].Id)
                    Program._indirizziRicerca.Add(Program._indirizzi[i]);
            }
            
        }
        #endregion
    }
}
