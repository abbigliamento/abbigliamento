﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NegozioAbbigliamento
{
    public class ClsAcquistare
    {
       
        #region Attributi
        //SABBATINI
        DateTime _dataOra;
        short _prezzo;

        //chiavi esterne, manca proprietà
        string _emailCliente;
        string _nomeProdottoCatalogo;
        #endregion
        #region Proprieta

        public DateTime DataOra { get => _dataOra; set => _dataOra = value; }
        public short Prezzo
        {
            get
            {
                return _prezzo;
            }
            set
            {
                if (value > 0)
                {
                    _prezzo = value;
                }
                else
                    value = _prezzo;
            }
        }
        #endregion
        #region Costruttore

        public ClsAcquistare()
        {
            
        }
        #endregion
        #region Metodi
        public ClsAcquistare(DateTime dataOra, short prezzo)
        {
            this.DataOra = dataOra;
            this.Prezzo = prezzo;
        }
        public void visualizzaCarrello(string email)
        {
            Program._acquistoSpecificoCliente.Clear();
            for (int i = 0; i < Program._acquisti.Count; i++)
            {
                if (email == Program._acquisti[i]._emailCliente)
                    Program._acquistoSpecificoCliente.Add(Program._acquisti[i]);
            }
        }
        #endregion

    }
}
