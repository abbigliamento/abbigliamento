﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NegozioAbbigliamento
{
    public class ClsBrand
    {
        
        #region Attributi
        //SABBATINI
        string _nome;
        #endregion
        #region Proprieta
        public string Nome
        {
            get
            {
                return _nome;
            }
            set
            {
                if (value.Length > 0)
                    _nome = value;
                else
                    value = _nome;
            }
        }
        #endregion
        #region Costruttore
        public ClsBrand()
        {

        }
        #endregion
        public ClsBrand(string _nome)
        {
            this.Nome = _nome;
        }
        #region Metodi

        #endregion
    }
}
