﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NegozioAbbigliamento
{

    public class ClsProdottoFisico
    {
        
        #region attributi
        //SABBATINI
        string _codice;

        //chiave esterna, manca proprietà
        string _nomeProdottoCatalogo;
        #endregion
        #region Proprieta
        public string Codice
        {
            get
            {
                return _codice;
            }
            set
            {
                if (value.Length > 0)
                {
                    _codice = value;
                }
                else
                    value = _codice;
            }
        }
        #endregion
        #region Costruttore
        public ClsProdottoFisico()
        {
            
        }
        #endregion
        #region Metodi
        public ClsProdottoFisico(string _codice)
        {
            this.Codice = _codice;
        }
        public void Elimina(string codice)
        {
            for (int i = 0; i < Program._prodottoFisico.Count; i++)
            {
                if (Program._prodottoFisico[i].Codice == codice)
                    Program._prodottoFisico.RemoveAt(i);

            }
        }
        #endregion
    }
}
