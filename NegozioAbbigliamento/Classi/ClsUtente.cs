﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NegozioAbbigliamento
{
    public class ClsUtente
    {
        #region Attributi
        //attributi
        //MONSU E SABBATINI
        DateTime _dataNascita;
        string _cognome, _nome, _email, _password;
        bool _registrazione;
        char[] caratteriSpeciali = {'&', '%', '/', '@', '#', '*','?','!','£'};
        #endregion
        #region Proprieta
        //proprieta
        public bool Registrazione { get => _registrazione; set => _registrazione = value; }
        public string Cognome { get => _cognome; set => _cognome = value; }
        public string Nome { get => _nome; set => _nome = value; }
        public string Email { get => _email; set => _email = value; }
        public string Password
        {
            get
            {
                return _password;

            }
            set
            {
                if (value.Length > 8)
                {
                    foreach (char carattere in value)
                    {
                        if (!Array.Exists(caratteriSpeciali, c => c == carattere)) //controllo se all'interno della password è presente almeno un carattere speciale
                            _password = value;
                    }
                }
                else
                    value = _password;

            }
        }

        public DateTime DataNascita { get => _dataNascita; set => _dataNascita = value; }

        #endregion
        #region Costruttore
        //costruttore

        public ClsUtente(string cognome, string nome, DateTime dataNascita, string email, string password)
        {
            this.Nome = nome;
            this.Cognome = cognome;
            this.Email = email;
            this.Password = password;
            this.DataNascita = dataNascita;
        }

        public ClsUtente()
        {
            
        }
        #endregion
        #region Metodi
        //metodi

        public bool accedi(string email)
        {
            for (int i = 0; i < Program._utenti.Count; i++)
            {
                if (Program._utenti[i].Email == email)
                    return true;
            }
            return false;
        }
        public void elimina(string email)
        {
            for (int i = 0; i < Program._utenti.Count; i++)
            {
                if (Program._utenti[i].Email == email)
                    Program._utenti.RemoveAt(i);
            }
        }
        public void ricerca(string email)
        {
            Program._utentiRicerca.Clear();
            for (int i = 0; i < Program._utenti.Count; i++)
            {
                if (email == Program._utenti[i].Email)
                    Program._utentiRicerca.Add(Program._utenti[i]);
            }
            
        }
        #endregion
    }
}
