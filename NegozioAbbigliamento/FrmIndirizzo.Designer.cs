﻿namespace NegozioAbbigliamento
{
    partial class FrmIndirizzo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlDatiConsegna = new System.Windows.Forms.Panel();
            this.tbVia = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.mtbCAP = new System.Windows.Forms.MaskedTextBox();
            this.tbStato = new System.Windows.Forms.TextBox();
            this.btnAnnulla = new System.Windows.Forms.Button();
            this.btnSalva = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbCitta = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDatiConsegna.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDatiConsegna
            // 
            this.pnlDatiConsegna.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlDatiConsegna.Controls.Add(this.tbVia);
            this.pnlDatiConsegna.Controls.Add(this.label7);
            this.pnlDatiConsegna.Controls.Add(this.mtbCAP);
            this.pnlDatiConsegna.Controls.Add(this.tbStato);
            this.pnlDatiConsegna.Controls.Add(this.btnAnnulla);
            this.pnlDatiConsegna.Controls.Add(this.btnSalva);
            this.pnlDatiConsegna.Controls.Add(this.label6);
            this.pnlDatiConsegna.Controls.Add(this.tbCitta);
            this.pnlDatiConsegna.Controls.Add(this.label3);
            this.pnlDatiConsegna.Controls.Add(this.label2);
            this.pnlDatiConsegna.Controls.Add(this.label1);
            this.pnlDatiConsegna.Location = new System.Drawing.Point(271, 6);
            this.pnlDatiConsegna.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlDatiConsegna.Name = "pnlDatiConsegna";
            this.pnlDatiConsegna.Size = new System.Drawing.Size(457, 494);
            this.pnlDatiConsegna.TabIndex = 6;
            // 
            // tbVia
            // 
            this.tbVia.Location = new System.Drawing.Point(108, 276);
            this.tbVia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbVia.Name = "tbVia";
            this.tbVia.Size = new System.Drawing.Size(224, 22);
            this.tbVia.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(103, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 25);
            this.label7.TabIndex = 20;
            this.label7.Text = "Via";
            // 
            // mtbCAP
            // 
            this.mtbCAP.Location = new System.Drawing.Point(108, 357);
            this.mtbCAP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mtbCAP.Mask = "00000";
            this.mtbCAP.Name = "mtbCAP";
            this.mtbCAP.Size = new System.Drawing.Size(81, 22);
            this.mtbCAP.TabIndex = 18;
            // 
            // tbStato
            // 
            this.tbStato.Location = new System.Drawing.Point(108, 121);
            this.tbStato.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbStato.Name = "tbStato";
            this.tbStato.Size = new System.Drawing.Size(224, 22);
            this.tbStato.TabIndex = 17;
            // 
            // btnAnnulla
            // 
            this.btnAnnulla.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAnnulla.Location = new System.Drawing.Point(104, 409);
            this.btnAnnulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAnnulla.Name = "btnAnnulla";
            this.btnAnnulla.Size = new System.Drawing.Size(101, 28);
            this.btnAnnulla.TabIndex = 16;
            this.btnAnnulla.Text = "Annulla";
            this.btnAnnulla.UseVisualStyleBackColor = false;
            this.btnAnnulla.Click += new System.EventHandler(this.btnAnnulla_Click);
            // 
            // btnSalva
            // 
            this.btnSalva.BackColor = System.Drawing.SystemColors.Menu;
            this.btnSalva.Location = new System.Drawing.Point(226, 409);
            this.btnSalva.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSalva.Name = "btnSalva";
            this.btnSalva.Size = new System.Drawing.Size(101, 28);
            this.btnSalva.TabIndex = 15;
            this.btnSalva.Text = "Salva";
            this.btnSalva.UseVisualStyleBackColor = false;
            this.btnSalva.Click += new System.EventHandler(this.btnSalva_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(103, 320);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "CAP";
            // 
            // tbCitta
            // 
            this.tbCitta.Location = new System.Drawing.Point(103, 194);
            this.tbCitta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCitta.Name = "tbCitta";
            this.tbCitta.Size = new System.Drawing.Size(224, 22);
            this.tbCitta.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(99, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Città";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(103, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stato";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(375, 61);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dati consegna";
            // 
            // FrmIndirizzo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(1001, 514);
            this.Controls.Add(this.pnlDatiConsegna);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmIndirizzo";
            this.Text = "FrmIndirizzo";
            this.pnlDatiConsegna.ResumeLayout(false);
            this.pnlDatiConsegna.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlDatiConsegna;
        private System.Windows.Forms.TextBox tbVia;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox mtbCAP;
        private System.Windows.Forms.TextBox tbStato;
        private System.Windows.Forms.Button btnAnnulla;
        private System.Windows.Forms.Button btnSalva;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbCitta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}