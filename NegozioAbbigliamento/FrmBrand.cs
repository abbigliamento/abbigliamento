﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using MySqlConnector;

namespace NegozioAbbigliamento
{
    public partial class FrmBrand : Form
    {
        public FrmBrand()
        {
            InitializeComponent();
        }

        private void btAnnulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FrmBrand_Load(object sender, EventArgs e)
        {
            
        }

        private void btSalva_Click(object sender, EventArgs e)
        {
            //Vengono richiamati i metodi messi a disposizione dal DBManager
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "INSERT INTO brand (nome) values (@nome)";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@nome", tbNomeBrand.Text)
            };

            long num = dbm.GetAffectedRows(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
                MessageBox.Show($"LastID: {num}");
            else
                MessageBox.Show($"Errore: {errore}");
        }
    }
}
