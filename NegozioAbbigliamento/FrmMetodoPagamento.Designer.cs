﻿namespace NegozioAbbigliamento
{
    partial class FrmMetodoPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCarta = new System.Windows.Forms.Panel();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.btSalva = new System.Windows.Forms.Button();
            this.cbTipoMetodoPagamento = new System.Windows.Forms.ComboBox();
            this.dtpDataScadenza = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.tbTitolare = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbNumeroCarta = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlCarta.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCarta
            // 
            this.pnlCarta.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlCarta.Controls.Add(this.btAnnulla);
            this.pnlCarta.Controls.Add(this.btSalva);
            this.pnlCarta.Controls.Add(this.cbTipoMetodoPagamento);
            this.pnlCarta.Controls.Add(this.dtpDataScadenza);
            this.pnlCarta.Controls.Add(this.label6);
            this.pnlCarta.Controls.Add(this.tbTitolare);
            this.pnlCarta.Controls.Add(this.label5);
            this.pnlCarta.Controls.Add(this.tbNumeroCarta);
            this.pnlCarta.Controls.Add(this.label3);
            this.pnlCarta.Controls.Add(this.label2);
            this.pnlCarta.Controls.Add(this.label1);
            this.pnlCarta.Location = new System.Drawing.Point(145, 44);
            this.pnlCarta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlCarta.Name = "pnlCarta";
            this.pnlCarta.Size = new System.Drawing.Size(411, 460);
            this.pnlCarta.TabIndex = 4;
            // 
            // btAnnulla
            // 
            this.btAnnulla.BackColor = System.Drawing.SystemColors.Menu;
            this.btAnnulla.Location = new System.Drawing.Point(75, 372);
            this.btAnnulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(101, 28);
            this.btAnnulla.TabIndex = 16;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = false;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.SystemColors.Menu;
            this.btSalva.Location = new System.Drawing.Point(197, 372);
            this.btSalva.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(101, 28);
            this.btSalva.TabIndex = 15;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // cbTipoMetodoPagamento
            // 
            this.cbTipoMetodoPagamento.FormattingEnabled = true;
            this.cbTipoMetodoPagamento.Items.AddRange(new object[] {
            "Mastercard",
            "Visa",
            "PayPal"});
            this.cbTipoMetodoPagamento.Location = new System.Drawing.Point(75, 121);
            this.cbTipoMetodoPagamento.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTipoMetodoPagamento.Name = "cbTipoMetodoPagamento";
            this.cbTipoMetodoPagamento.Size = new System.Drawing.Size(224, 24);
            this.cbTipoMetodoPagamento.TabIndex = 14;
            // 
            // dtpDataScadenza
            // 
            this.dtpDataScadenza.Location = new System.Drawing.Point(75, 261);
            this.dtpDataScadenza.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpDataScadenza.Name = "dtpDataScadenza";
            this.dtpDataScadenza.Size = new System.Drawing.Size(224, 22);
            this.dtpDataScadenza.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(69, 233);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(163, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "Data di scadenza";
            // 
            // tbTitolare
            // 
            this.tbTitolare.Location = new System.Drawing.Point(75, 324);
            this.tbTitolare.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbTitolare.Name = "tbTitolare";
            this.tbTitolare.Size = new System.Drawing.Size(224, 22);
            this.tbTitolare.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(75, 295);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 25);
            this.label5.TabIndex = 8;
            this.label5.Text = "Titolare";
            // 
            // tbNumeroCarta
            // 
            this.tbNumeroCarta.Location = new System.Drawing.Point(75, 194);
            this.tbNumeroCarta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNumeroCarta.Name = "tbNumeroCarta";
            this.tbNumeroCarta.Size = new System.Drawing.Size(224, 22);
            this.tbNumeroCarta.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(69, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Numero";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(69, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tipo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(350, 61);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dettagli carta";
            // 
            // FrmMetodoPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(689, 554);
            this.Controls.Add(this.pnlCarta);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmMetodoPagamento";
            this.Text = "MetodoPagamento";
            this.Load += new System.EventHandler(this.FrmMetodoPagamento_Load);
            this.pnlCarta.ResumeLayout(false);
            this.pnlCarta.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCarta;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.ComboBox cbTipoMetodoPagamento;
        private System.Windows.Forms.DateTimePicker dtpDataScadenza;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbTitolare;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbNumeroCarta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}