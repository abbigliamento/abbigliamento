﻿namespace NegozioAbbigliamento
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbCategorie = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btAggiungiCarrello = new System.Windows.Forms.Button();
            this.btEsci = new System.Windows.Forms.Button();
            this.dgvVisualizza = new System.Windows.Forms.DataGridView();
            this.btCerca = new System.Windows.Forms.Button();
            this.tbCercaProdotto = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisualizza)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(225, 439);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(281, 38);
            this.label3.TabIndex = 11;
            this.label3.Text = "Visualizza carrello";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(55, 421);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(153, 123);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(369, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 38);
            this.label2.TabIndex = 9;
            this.label2.Text = "Prodotti";
            // 
            // lbCategorie
            // 
            this.lbCategorie.BackColor = System.Drawing.Color.White;
            this.lbCategorie.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lbCategorie.FormattingEnabled = true;
            this.lbCategorie.ItemHeight = 16;
            this.lbCategorie.Location = new System.Drawing.Point(55, 57);
            this.lbCategorie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbCategorie.Name = "lbCategorie";
            this.lbCategorie.Size = new System.Drawing.Size(135, 340);
            this.lbCategorie.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 38);
            this.label1.TabIndex = 7;
            this.label1.Text = "Categorie";
            // 
            // btAggiungiCarrello
            // 
            this.btAggiungiCarrello.BackColor = System.Drawing.SystemColors.Menu;
            this.btAggiungiCarrello.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAggiungiCarrello.Location = new System.Drawing.Point(589, 402);
            this.btAggiungiCarrello.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAggiungiCarrello.Name = "btAggiungiCarrello";
            this.btAggiungiCarrello.Size = new System.Drawing.Size(164, 60);
            this.btAggiungiCarrello.TabIndex = 13;
            this.btAggiungiCarrello.Text = "Aggiungi al carrello";
            this.btAggiungiCarrello.UseVisualStyleBackColor = false;
            this.btAggiungiCarrello.Click += new System.EventHandler(this.btAggiungiCarrello_Click);
            // 
            // btEsci
            // 
            this.btEsci.BackColor = System.Drawing.SystemColors.Menu;
            this.btEsci.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEsci.Location = new System.Drawing.Point(1037, 528);
            this.btEsci.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEsci.Name = "btEsci";
            this.btEsci.Size = new System.Drawing.Size(67, 30);
            this.btEsci.TabIndex = 14;
            this.btEsci.Text = "Esci";
            this.btEsci.UseVisualStyleBackColor = false;
            this.btEsci.Click += new System.EventHandler(this.btEsci_Click);
            // 
            // dgvVisualizza
            // 
            this.dgvVisualizza.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvVisualizza.BackgroundColor = System.Drawing.Color.White;
            this.dgvVisualizza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVisualizza.Location = new System.Drawing.Point(213, 57);
            this.dgvVisualizza.Margin = new System.Windows.Forms.Padding(4);
            this.dgvVisualizza.Name = "dgvVisualizza";
            this.dgvVisualizza.Size = new System.Drawing.Size(540, 339);
            this.dgvVisualizza.TabIndex = 15;
            // 
            // btCerca
            // 
            this.btCerca.BackColor = System.Drawing.SystemColors.Menu;
            this.btCerca.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCerca.Location = new System.Drawing.Point(971, 87);
            this.btCerca.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btCerca.Name = "btCerca";
            this.btCerca.Size = new System.Drawing.Size(75, 32);
            this.btCerca.TabIndex = 16;
            this.btCerca.Text = "Cerca";
            this.btCerca.UseVisualStyleBackColor = false;
            this.btCerca.Click += new System.EventHandler(this.btCerca_Click);
            // 
            // tbCercaProdotto
            // 
            this.tbCercaProdotto.Location = new System.Drawing.Point(916, 60);
            this.tbCercaProdotto.Name = "tbCercaProdotto";
            this.tbCercaProdotto.Size = new System.Drawing.Size(130, 22);
            this.tbCercaProdotto.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(786, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "Nome prodotto:";
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(1104, 558);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbCercaProdotto);
            this.Controls.Add(this.btCerca);
            this.Controls.Add(this.dgvVisualizza);
            this.Controls.Add(this.btEsci);
            this.Controls.Add(this.btAggiungiCarrello);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbCategorie);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmMenu";
            this.Text = "FrmMenu";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisualizza)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lbCategorie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btAggiungiCarrello;
        private System.Windows.Forms.Button btEsci;
        private System.Windows.Forms.DataGridView dgvVisualizza;
        private System.Windows.Forms.Button btCerca;
        private System.Windows.Forms.TextBox tbCercaProdotto;
        private System.Windows.Forms.Label label4;
    }
}