﻿namespace NegozioAbbigliamento
{
    partial class FrmCategoria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCategoria = new System.Windows.Forms.Panel();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.tbNomeCategoria = new System.Windows.Forms.TextBox();
            this.btSalva = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlCategoria.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCategoria
            // 
            this.pnlCategoria.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlCategoria.Controls.Add(this.btAnnulla);
            this.pnlCategoria.Controls.Add(this.tbNomeCategoria);
            this.pnlCategoria.Controls.Add(this.btSalva);
            this.pnlCategoria.Controls.Add(this.label2);
            this.pnlCategoria.Controls.Add(this.label1);
            this.pnlCategoria.Location = new System.Drawing.Point(209, 94);
            this.pnlCategoria.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlCategoria.Name = "pnlCategoria";
            this.pnlCategoria.Size = new System.Drawing.Size(383, 263);
            this.pnlCategoria.TabIndex = 3;
            // 
            // btAnnulla
            // 
            this.btAnnulla.BackColor = System.Drawing.SystemColors.Menu;
            this.btAnnulla.Location = new System.Drawing.Point(77, 166);
            this.btAnnulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(101, 28);
            this.btAnnulla.TabIndex = 8;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = false;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // tbNomeCategoria
            // 
            this.tbNomeCategoria.Location = new System.Drawing.Point(77, 138);
            this.tbNomeCategoria.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNomeCategoria.Name = "tbNomeCategoria";
            this.tbNomeCategoria.Size = new System.Drawing.Size(224, 22);
            this.tbNomeCategoria.TabIndex = 3;
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.SystemColors.Menu;
            this.btSalva.Location = new System.Drawing.Point(200, 166);
            this.btSalva.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(101, 28);
            this.btSalva.TabIndex = 7;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(75, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(59, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 61);
            this.label1.TabIndex = 0;
            this.label1.Text = "Categoria";
            // 
            // FrmCategoria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnlCategoria);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmCategoria";
            this.Text = "FrmCategoria";
            this.Load += new System.EventHandler(this.FrmCategoria_Load);
            this.pnlCategoria.ResumeLayout(false);
            this.pnlCategoria.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCategoria;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.TextBox tbNomeCategoria;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}