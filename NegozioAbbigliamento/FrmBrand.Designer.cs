﻿namespace NegozioAbbigliamento
{
    partial class FrmBrand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBrand = new System.Windows.Forms.Panel();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.btSalva = new System.Windows.Forms.Button();
            this.tbNomeBrand = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBrand.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBrand
            // 
            this.pnlBrand.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlBrand.Controls.Add(this.btAnnulla);
            this.pnlBrand.Controls.Add(this.btSalva);
            this.pnlBrand.Controls.Add(this.tbNomeBrand);
            this.pnlBrand.Controls.Add(this.label2);
            this.pnlBrand.Controls.Add(this.label1);
            this.pnlBrand.Location = new System.Drawing.Point(209, 103);
            this.pnlBrand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlBrand.Name = "pnlBrand";
            this.pnlBrand.Size = new System.Drawing.Size(383, 245);
            this.pnlBrand.TabIndex = 5;
            // 
            // btAnnulla
            // 
            this.btAnnulla.BackColor = System.Drawing.SystemColors.Menu;
            this.btAnnulla.Location = new System.Drawing.Point(80, 164);
            this.btAnnulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(101, 28);
            this.btAnnulla.TabIndex = 6;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = false;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.SystemColors.Menu;
            this.btSalva.Location = new System.Drawing.Point(203, 164);
            this.btSalva.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(101, 28);
            this.btSalva.TabIndex = 5;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // tbNomeBrand
            // 
            this.tbNomeBrand.Location = new System.Drawing.Point(80, 121);
            this.tbNomeBrand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNomeBrand.Name = "tbNomeBrand";
            this.tbNomeBrand.Size = new System.Drawing.Size(224, 22);
            this.tbNomeBrand.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(77, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(75, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 61);
            this.label1.TabIndex = 0;
            this.label1.Text = "Brand";
            // 
            // FrmBrand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnlBrand);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmBrand";
            this.Text = "FrmBrand";
            this.Load += new System.EventHandler(this.FrmBrand_Load);
            this.pnlBrand.ResumeLayout(false);
            this.pnlBrand.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBrand;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.TextBox tbNomeBrand;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}