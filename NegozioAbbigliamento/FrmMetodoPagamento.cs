﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using MySqlConnector;

namespace NegozioAbbigliamento
{
    public partial class FrmMetodoPagamento : Form
    {
        private string clienteEmail;

        public FrmMetodoPagamento(string email)
        {
            InitializeComponent();
            this.clienteEmail = email; // Memorizza l'email
        }

        private void btAnnulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btSalva_Click(object sender, EventArgs e)
        {
            //Vengono richiamati i metodi messi a disposizione dal DBManager
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "INSERT INTO metodipagamento (tipo, numeroCarta, scadenza, titolare, clienteEmail) values (@tipo, @numeroCarta, @scadenza, @titolare, @clienteEmail)";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@tipo", cbTipoMetodoPagamento.SelectedItem),
                new MySqlParameter("@numeroCarta", tbNumeroCarta.Text),
                new MySqlParameter("@scadenza", dtpDataScadenza.Value),
                new MySqlParameter("@titolare", tbTitolare.Text),
                new MySqlParameter("@clienteEmail", this.clienteEmail)
            };

            long num = dbm.GetAffectedRows(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
                MessageBox.Show($"LastID: {num}");
            else
                MessageBox.Show($"Errore: {errore}");
        }
        private void FrmMetodoPagamento_Load(object sender, EventArgs e)
        {
        
        }
    }
}
