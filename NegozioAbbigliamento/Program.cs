﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NegozioAbbigliamento
{
    
    public static class Program
    {
        public static List<ClsUtente> _utenti = new List<ClsUtente>();
        public static List<ClsUtente> _utentiRicerca = new List<ClsUtente>();
        public static List<ClsIndirizzo> _indirizzi = new List<ClsIndirizzo>();
        public static List<ClsCliente> _clienti = new List<ClsCliente>();
        public static List<ClsAdmin> _admins = new List<ClsAdmin>();
        public static List<ClsIndirizzo> _indirizziRicerca = new List<ClsIndirizzo>();
        public static List<ClsMetodoPagamento> _metodiPagamento = new List<ClsMetodoPagamento>();
        public static List<ClsMetodoPagamento> _metodiPagamentoRicerca = new List<ClsMetodoPagamento>();
        public static List<ClsCategoria> _categorie = new List<ClsCategoria>();
        public static List<ClsCategoria> _cateogireRicerca = new List<ClsCategoria>();
        public static List<ClsBrand> _brand = new List<ClsBrand>();
        public static List<ClsProdottoFisico> _prodottoFisico = new List<ClsProdottoFisico>();
        public static List<ClsProdottoCatalogo> _prodottoCatalogo = new List<ClsProdottoCatalogo>();
        public static List<ClsProdottoCatalogo> _prodottoCatalogoRicerca = new List<ClsProdottoCatalogo>();
        public static List<ClsAcquistare> _acquisti = new List<ClsAcquistare>();
        public static List<ClsAcquistare> _acquistoSpecificoCliente = new List<ClsAcquistare>();
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmAccesso());
        }
    }
}
