﻿namespace NegozioAbbigliamento
{
    partial class FrmInsiemeOrdini
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvInsiemeOrdini = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.btInserisciCategoria = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvInsiemeOrdini
            // 
            this.lvInsiemeOrdini.HideSelection = false;
            this.lvInsiemeOrdini.Location = new System.Drawing.Point(54, 62);
            this.lvInsiemeOrdini.Margin = new System.Windows.Forms.Padding(2);
            this.lvInsiemeOrdini.Name = "lvInsiemeOrdini";
            this.lvInsiemeOrdini.Size = new System.Drawing.Size(391, 277);
            this.lvInsiemeOrdini.TabIndex = 14;
            this.lvInsiemeOrdini.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(167, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 51);
            this.label1.TabIndex = 15;
            this.label1.Text = "Ordini";
            // 
            // btInserisciCategoria
            // 
            this.btInserisciCategoria.BackColor = System.Drawing.SystemColors.Menu;
            this.btInserisciCategoria.Location = new System.Drawing.Point(176, 343);
            this.btInserisciCategoria.Margin = new System.Windows.Forms.Padding(2);
            this.btInserisciCategoria.Name = "btInserisciCategoria";
            this.btInserisciCategoria.Size = new System.Drawing.Size(137, 41);
            this.btInserisciCategoria.TabIndex = 16;
            this.btInserisciCategoria.Text = "CHIUDI";
            this.btInserisciCategoria.UseVisualStyleBackColor = false;
            this.btInserisciCategoria.Click += new System.EventHandler(this.btInserisciCategoria_Click);
            // 
            // FrmInsiemeOrdini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(506, 415);
            this.Controls.Add(this.btInserisciCategoria);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvInsiemeOrdini);
            this.Name = "FrmInsiemeOrdini";
            this.Text = "FrmInsiemeOrdini";
            this.Load += new System.EventHandler(this.FrmInsiemeOrdini_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvInsiemeOrdini;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btInserisciCategoria;
    }
}