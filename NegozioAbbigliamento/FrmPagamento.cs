﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using MySqlConnector;

namespace NegozioAbbigliamento
{
    public partial class FrmPagamento : Form
    {
        private string clienteEmail;

        public FrmPagamento(string email)
        {
            InitializeComponent();
            this.clienteEmail = email; // Memorizza l'email
        }

        private void btAggiungiIndirizzo_Click(object sender, EventArgs e)
        {
            FrmIndirizzo frmIndirizzo = new FrmIndirizzo(this.clienteEmail);
            DialogResult dr = frmIndirizzo.ShowDialog(this);

            if (dr == DialogResult.OK || dr == DialogResult.Cancel) // Quando chiudi la finestra, ricarica i dati
            {
                IndirizzoCliente(); // Ricarica l'elenco degli indirizzi
            }
        }

        private void btAggiungiCarta_Click(object sender, EventArgs e)
        {
            FrmMetodoPagamento frmMetodoPagamento = new FrmMetodoPagamento(this.clienteEmail);
            DialogResult dr = frmMetodoPagamento.ShowDialog(this);

            if (dr == DialogResult.OK || dr == DialogResult.Cancel) // Quando chiudi la finestra, ricarica i dati
            {
                CartaCliente(); // Ricarica l'elenco delle carte
            }
        }
        private void FrmPagamento_Load(object sender, EventArgs e)
        {
            IndirizzoCliente();
            CartaCliente();
        }

        private void CartaCliente()
        {
            DBManager dbm = new DBManager();
            string errore = "";

            string sql = "SELECT numeroCarta, titolare FROM metodipagamento WHERE clienteEmail = @clienteEmail";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@clienteEmail", this.clienteEmail)
            };

            DataTable dt = dbm.GetDataTableByQuery(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
            {
                //aggiungiamo uan colonna alla datatable in modo da contenere sia numero carta che titolare
                dt.Columns.Add("Descrizione", typeof(string));

                foreach (DataRow riga in dt.Rows)
                {
                    riga["Descrizione"] = riga["numeroCarta"].ToString() + " - " + riga["titolare"].ToString();
                }

                cbCarta.DataSource = dt;
                cbCarta.DisplayMember = "Descrizione"; //quello che vediamo sulla combobox
            }
            else
            {
                MessageBox.Show($"Errore: {errore}");
            }
        }


        private void IndirizzoCliente()
        {
            DBManager dbm = new DBManager();
            string errore = "";

            string sql = "SELECT via, citta FROM indirizzi WHERE clienteEmail = @clienteEmail";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@clienteEmail", this.clienteEmail)
            };

            DataTable dt = dbm.GetDataTableByQuery(sql, parametri, ref errore); 

            if (string.IsNullOrEmpty(errore))
            {
                //aggiungiamo uan colonna alla datatable in modo da contenere sia via che città
                dt.Columns.Add("Descrizione", typeof(string));

                foreach (DataRow riga in dt.Rows)
                {
                    riga["Descrizione"] = riga["via"].ToString() + " - " + riga["citta"].ToString();
                }

                cbIndirizzo.DataSource = dt;
                cbIndirizzo.DisplayMember = "Descrizione"; //quello che vediamo sulla combobox
            }
            else
            {
                MessageBox.Show($"Errore: {errore}");
            }
        }

        private void btOrdinaCarta_Click(object sender, EventArgs e)
        {

        }

        private void btSelezionaIndirizzo_Click(object sender, EventArgs e)
        {

        }
    }
}
